package mconf

import (
	"fmt"
	"mailer/pkg/mailer"
	"testing"
)

func TestSubject(t *testing.T) {

	f := mailer.Form{
		Name:      "Valid Name",
		Email:     "valid@email.com",
		Telephone: "(99) 999999999",
		Message:   "Mensagem Válida",
	}

	if subject(f) != fmt.Sprintf("Nova mensagem de %sno site da WeNova", f.Name) {
		t.Error("Expected subject to correctly concatenate the f.Name")
	}
}

func TestWrite(t *testing.T) {
	f := mailer.Form{
		Name:      "Valid Name",
		Email:     "valid@email.com",
		Telephone: "(99) 999999999",
		Message:   "Mensagem Válida",
	}

	if write(f) != fmt.Sprintf("Email: %s\nNome: %s\nTelefone: %s\nMensagem: %s\n",
		f.Email, f.Name, f.Telephone, f.Message) {
		t.Error("Expected body to correctly concatenate the form values")
	}
}
