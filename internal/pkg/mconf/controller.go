package mconf

import (
	"errors"
	"fmt"
	"mailer/pkg/mailer"
	"os"
)

// subject uses the Name in mailer.Form to
// generate a customized subject to the e-mail
func subject(f mailer.Form) string {
	return fmt.Sprintf("Nova mensagem de %s no site da WeNova", f.Name)

}

// write uses all the information in the
// mailer.Form to mount the body of the e-mail
// that is expected on the mailer pkg
func write(f mailer.Form) string {
	return fmt.Sprintf("Email: %s\nNome: %s\nTelefone: %s\nMensagem: %s\n",
		f.Email, f.Name, f.Telephone, f.Message)
}

// SendMail insert the Subject and Body to a mailer.Handler
// And then it send the handler to sendMail from the mailer
// package
func SendMail(f mailer.Form, mh *mailer.Handler) {
	mh.Body = write(f)
	mh.Subject = subject(f)

	mailer.SendMail(*mh)
}

//Create is exported so a mailer handler can be created
// in the command, with the rights specifications
func Create() (*mailer.Handler, error) {
	return create()
}

// create is responsible to create the mailer.Handler
// it returns a pointer to mailer.Handler and an error
// the error is not nil if the environment is not set
func create() (*mailer.Handler, error) {
	// Return the function if the environment path is not set
	if os.Getenv("mailersender") == "" || os.Getenv("maileraddress") == "" {
		return nil, errors.New("environment path not set, please ensure you have a mailersender and maileraddress variable exported")
	}
	// Create a pointer to mailer.Handler with the values
	// of the variables mailersender and maileraddress
	h := mailer.Handler{
		SenderID:  os.Getenv("mailersender"),
		AddressID: []string{os.Getenv("maileraddress")},
	}
	// returns the value of the mailer.Handler
	return &h, nil
}
