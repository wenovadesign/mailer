package nconf

import (
	"time"

	"github.com/jinzhu/gorm"
	// i need postgres
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type conn struct {
	Host, Port, User, DBName, Password string
	SSL                                bool
}

type subscriber struct {
	gorm.Model
	Email        string `sql:"size:135;unique;index"`
	RegisterDate time.Time
}

type Subscriber struct {
	Email string
}
