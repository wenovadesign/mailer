package nconf

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	//Import Postgres dialect to use with gorm
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Save receives a variable of type string
// and save it in the database witha  collumn to the
// timestamp
func Save(e string) {
	save(e)
	return
}

func save(e string) {
	db := dbConn()
	defer db.Close()
	now := time.Now()
	db.AutoMigrate(&subscriber{})
	db.Create(&subscriber{Email: e, RegisterDate: now})
}

// ShowAll returns a list with all the registered emails
// with its created data
func ShowAll(e *json.Encoder) {
	showAll(e)
	return
}

func showAll(e *json.Encoder) {
	//var s subscriber
	db := dbConn()
	defer db.Close()
	s := []subscriber{}
	db.Find(&s)
	//subs, err := json.Marshal(s)
	err := e.Encode(s)
	if err != nil {
		log.Println(err)
	}
	/*
		for item := range s {
			email := mailer.Newsletter{Email: item.Email}
			sn = append(sn, email)
		}
	*/
	return
}

func dbConn() *gorm.DB {
	c := conn{
		Host:     os.Getenv("db_host"),
		Port:     os.Getenv("db_port"),
		User:     os.Getenv("db_user"),
		DBName:   os.Getenv("db_name"),
		Password: os.Getenv("db_pass"),
		SSL:      false,
	}
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", c.Host, c.Port, c.User, c.DBName, c.Password))
	if err != nil {
		panic(err)
	}
	return db
}
