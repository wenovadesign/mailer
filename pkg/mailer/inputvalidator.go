package mailer

import (
	"regexp"
)

// FormInput type to hold validator of all kinds of form input
type FormInput interface {
	IsValid() bool
}

//Types

// FormName type to validate name strings
type FormName string

// FormEmail type to validate email strings
type FormEmail string

// FormTelephone type to validate telephone strings
type FormTelephone string

// FormMessage type to validate message strings and remove malicious input
type FormMessage string

//Functions

func (n FormName) IsValid() bool {
	var v bool
	// Names can't contain numbers, can't have the following: ',.-"
	// at the beggining of it.
	r, _ := regexp.Compile("^[\\p{L}\\s'.-]+$")
	v = r.MatchString(string(n))
	return v
}

func (e FormEmail) IsValid() bool {
	var v bool
	r, _ := regexp.Compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|'(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*')@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
	v = r.MatchString(string(e))
	return v
}

func (t FormTelephone) IsValid() bool {
	var v bool
	r, _ := regexp.Compile("^(?:(?:\\+|00)?(55)\\s?)?(?:\\(?([1-9][0-9])\\)?\\s?)?(?:((?:9\\d|[2-9])\\d{3})\\-?(\\d{4}))$")
	v = r.MatchString(string(t))
	return v
}

func (m FormMessage) IsValid() bool {
	var v bool
	r, _ := regexp.Compile("</?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[\\^'\">\\s]+))?)+\\s*|\\s*)/?>")
	v = !r.MatchString(string(m))
	return v
}
