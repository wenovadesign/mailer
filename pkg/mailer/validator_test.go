package mailer

import "testing"

func TestValidName(t *testing.T) {
	names := []FormName{"Peter Gopher", "João Gopher", "Gopher", "goPher", "Mc'Gopher"}

	for _, n := range names {
		if n.IsValid() == false {
			t.Error("Expected isValid to return true")
		}
	}
}

func TestInvalidName(t *testing.T) {
	names := []FormName{"Pe1er Gopher", "Joã&o Gopher", "Gop2her", "goP_her", "Mc+Gopher"}

	for _, n := range names {
		if n.IsValid() == true {
			t.Error("Expected isValid to return false")
		}
	}
}

func TestValidEmail(t *testing.T) {
	emails := []FormEmail{"peter@gopher.com", "jose@gopher.com.br", "perdomo@likeboys.net", "munnique2321.23@gmail23.com", "music@gopher.br", "none@whocares.ch"}

	for _, e := range emails {
		if e.IsValid() == false {
			t.Error("Expected isValid to return true")
		}
	}
}

func TestInvalidEmail(t *testing.T) {
	emails := []FormEmail{"peter@gopher", "@.com", "gmail@@com", "gmail@@.com"}

	for _, e := range emails {
		if e.IsValid() == true {
			t.Error("Expected isValid to return false")
		}
	}
}

func TestValidTelephone(t *testing.T) {
	phones := []FormTelephone{"21999999999", "(21)99999-9999", "(21) 999999999", "(99) 9999-9999"}

	for _, p := range phones {
		if p.IsValid() == false {
			t.Error("Expected isValid to return true")
		}
	}
}

func TestInvalidTelephone(t *testing.T) {
	phones := []FormTelephone{"peter@gopher", "999999999999999999", "(555) 218992923", "(999) 999999999"}

	for _, p := range phones {
		if p.IsValid() == true {
			t.Error("Expected isValid to return false")
		}
	}
}

func TestValidMessage(t *testing.T) {
	messages := []FormMessage{"21999999999", "Olá meu nome é Lucão da rapaziada", "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|'(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*')@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", "(21) 999999999", "(99) 9999-9999"}

	for _, m := range messages {
		if m.IsValid() == false {
			t.Error("Expected isValid to return true")
		}
	}
}

func TestInvalidMessage(t *testing.T) {
	messages := []FormMessage{"<script></script>", "<html>", "</html>"}

	for _, m := range messages {
		if m.IsValid() == true {
			t.Error("Expected isValid to return false")
		}
	}
}
