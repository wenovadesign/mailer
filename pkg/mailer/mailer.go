package mailer

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/smtp"
	"os"
	"strings"
)

// Handler is the email skeleton
type Handler struct {
	SenderID  string
	AddressID []string
	Subject   string
	Body      string
}

// SMTP is the skeleton with info needed to send emails
type SMTP struct {
	host string
	port string
}

// ServerName is responsible to return the name of the server
func (s *SMTP) ServerName() string {
	return s.host + ":" + s.port
}

// BuildMessage is the method responsible to correctly construct the email
func (mail *Handler) BuildMessage() string {
	message := ""
	message += fmt.Sprintf("From: %s\r\n", mail.SenderID)
	if len(mail.AddressID) > 0 {
		message += fmt.Sprintf("To: %s\r\n", strings.Join(mail.AddressID, ";"))
	}
	message += fmt.Sprintf("Subject: %s\r\n", mail.Subject)
	message += "\r\n" + mail.Body

	return message
}

// SendMail is the responsible to send the messages
func SendMail(m Handler) {
	messageBody := m.BuildMessage()
	log.Println("Done!")
	SMTP := SMTP{host: "smtp.gmail.com", port: "465"}
	log.Println(SMTP.host)

	auth := smtp.PlainAuth("", m.SenderID, os.Getenv("mailerpass"), SMTP.host)

	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         SMTP.host,
	}

	conn, err := tls.Dial("tcp", SMTP.ServerName(), tlsconfig)
	if err != nil {
		log.Panic(err)
	}

	client, err := smtp.NewClient(conn, SMTP.host)
	if err != nil {
		log.Panic(err)
	}

	if err = client.Auth(auth); err != nil {
		log.Panic(err)
	}

	if err = client.Mail(m.SenderID); err != nil {
		log.Panic(err)
	}
	for _, k := range m.AddressID {
		if err = client.Rcpt(k); err != nil {
			log.Panic(err)
		}
	}

	w, err := client.Data()
	if err != nil {
		log.Panic(err)
	}

	_, err = w.Write([]byte(messageBody))
	if err != nil {
		log.Panic(err)
	}

	err = w.Close()
	if err != nil {
		log.Panic(err)
	}

	client.Quit()

	log.Println("Success!")
}
