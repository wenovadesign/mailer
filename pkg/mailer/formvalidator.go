package mailer

// FormValidator ...
type FormValidator interface {
	isFormValid() bool
}

// Form is the skeleton of a form
type Form struct {
	Name      FormName      `json:"name"`
	Email     FormEmail     `json:"email"`
	Telephone FormTelephone `json:"telephone"`
	Message   FormMessage   `json:"message"`
}

// Newsletter is the skeleton of a newsletter email registration
type Newsletter struct {
	Email FormEmail `json:"email"`
}

func (iv Form) isFormValid() bool {
	var v bool
	// para cada InputVali
	return v
}
