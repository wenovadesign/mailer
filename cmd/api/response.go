package main

import (
	"encoding/json"
	"log"
)

//response is a basic struct cotaining a name and statuscode to an error
type response struct {
	Message string
}

func errorResponse(n string, e *json.Encoder) {

	err := response{Message: n}
	log.Println(err)
	e.Encode(err)

}

func successResponse(n string, e *json.Encoder) {

	err := response{Message: n}
	log.Println(err)
	e.Encode(err)

}
