package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"mailer/internal/pkg/mconf"
	"mailer/internal/pkg/nconf"

	"mailer/pkg/mailer"
)

func main() {
	http.HandleFunc("/mailer", MailerRoute)
	http.HandleFunc("/newsletter", NewsletterRoute)
	port := ":" + os.Getenv("PORT")
	log.Println("Starting server, listening at port", port)
	http.ListenAndServe(port, nil)
}

func NewsletterRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")
	switch r.Method {
	case "OPTIONS":
		return
	case "POST":
		NewsletterRegister(w, r)
	case "GET":
		NewsletterRetrieve(w, r)
	}
}

func NewsletterRetrieve(w http.ResponseWriter, r *http.Request) {
	log.Println("Selecting emails from database")
	nconf.ShowAll(json.NewEncoder(w))
}

// NewsletterRegister reveices a JSON string containing
// An email to be registered
func NewsletterRegister(w http.ResponseWriter, r *http.Request) {
	//w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")
	log.Println("Email received")
	var n mailer.Newsletter
	log.Println(r.Body)
	if r.Body == nil {
		//w.WriteHeader(http.StatusNoContent)
		errorResponse("Data not found", json.NewEncoder(w))
		return
	}
	err := json.NewDecoder(r.Body).Decode(&n)
	if err != nil {
		//w.WriteHeader(http.StatusBadRequest)
		errorResponse("Data not found!", json.NewEncoder(w))
		return
	}
	log.Println(n.Email)
	/* code below to be improved */
	if n.Email.IsValid() == true {
		//w.WriteHeader(http.StatusOK)
		log.Println("All inputs are valid!")
		log.Println("Email:", n.Email)
		nconf.Save(string(n.Email))
		successResponse("Mail successfully stored", json.NewEncoder(w))
		return
	}
	//w.WriteHeader(http.StatusUnprocessableEntity)
	errorResponse("Invalid Email", json.NewEncoder(w))
	return
}

// MailerRoute receives a JSON containing the form type
// imported from the formvalidator package
// it validates the JSON input, and if its valid, the mail is
// sent
func MailerRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	if r.Method == "OPTIONS" {
		return
	}

	log.Println("JSON/Form received")
	var f mailer.Form
	if r.Body == nil {
		w.WriteHeader(http.StatusBadRequest)
		errorResponse("Data not found!", json.NewEncoder(w))
		return
	}
	err := json.NewDecoder(r.Body).Decode(&f)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		errorResponse("Data not found!", json.NewEncoder(w))
		return
	}
	log.Println("form:", f)
	/* code below to be improved */
	var validator bool
	log.Println("name:", f.Name.IsValid(), "email: ", f.Email.IsValid(), "phone :", f.Telephone.IsValid(), "mess :", f.Message.IsValid())
	validator = f.Name.IsValid() && f.Email.IsValid() && f.Telephone.IsValid() && f.Message.IsValid()
	handl, err := mconf.Create()
	if err != nil {
		panic(err)
	}

	if validator == true {
		nconf.Save(string(f.Email))
		log.Println("All inputs are valid!")
		mconf.SendMail(f, handl)
		w.WriteHeader(http.StatusOK)
		successResponse("Email successfully sent", json.NewEncoder(w))
	} else {
		w.WriteHeader(http.StatusUnprocessableEntity)
		errorResponse("Invalid inputs found!", json.NewEncoder(w))
		//http.Error(w, err.Error(), 422)
	}

}
